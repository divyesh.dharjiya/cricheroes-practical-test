import { Component, OnInit } from '@angular/core';
import * as data from 'public/data.json';
@Component({
  selector: 'app-question-one-answer-one',
  templateUrl: './question-one-answer-one.component.html',
  styleUrls: ['./question-one-answer-one.component.scss']
})
export class QuestionOneAnswerOneComponent implements OnInit {
  // points table data variables
  pointsTableData: any = [];
  treacherousData: any = {};
  rangnaRockStarData: any = {};
  // treachrous and rangnarock scores data variables
  treacherousEightMatchForScore: number = 120;
  treacherousEightMatchForOvers: number = 14.333;
  rangnaRockStarEightMatchForScore: number = 119;
  rangnaRockStarEightMatchForOvers: number = 20;
  // treachrous for and against data variables
  treachrousForRuns: number;
  treachrousForOvers: number;
  treachrousAgainstRuns: number;
  treachrousAgainstOvers: number;
  // rangnarock for and against data variables
  rangnaRockStarForRuns: number;
  rangnaRockStarForOvers: number;
  rangnaRockStarAgainstRuns: number;
  rangnaRockStarAgainstOvers: number;
  // treachrous and rangnarock NRR variables
  treachrousNrr: number;
  rangnaRockStarNrr: number;
  constructor() { 
    this.pointsTableData = data['default'];
    this.pointsTableData.forEach((item) => {
      if (item.team === 'Treacherous') {
        this.treacherousData = item;
      } else if (item.team === 'RAGNAROK STARS') {
        this.rangnaRockStarData = item;
      }
    });
  }

  ngOnInit(): void {
    this.getAllCalculations();
  }

  getAllCalculations() {
    this.treachrousForRuns = this.treacherousData.forRuns + this.treacherousEightMatchForScore;
    this.treachrousForOvers = this.treacherousData.forOvers + this.treacherousEightMatchForOvers;
    this.treachrousAgainstRuns = this.treacherousData.againstRuns + this.rangnaRockStarEightMatchForScore;
    this.treachrousAgainstOvers = this.treacherousData.againstOvers + this.rangnaRockStarEightMatchForOvers;
    this.rangnaRockStarForRuns = this.rangnaRockStarData.forRuns + this.rangnaRockStarEightMatchForScore;
    this.rangnaRockStarForOvers = this.rangnaRockStarData.forOvers + this.rangnaRockStarEightMatchForOvers;
    this.rangnaRockStarAgainstRuns = this.rangnaRockStarData.againstRuns + this.treacherousEightMatchForScore;
    this.rangnaRockStarAgainstOvers = this.rangnaRockStarData.againstOvers + this.treacherousEightMatchForOvers;

    if (this.rangnaRockStarEightMatchForScore === this.treacherousEightMatchForScore) {
      // match Tied
      console.log('match tied');
      this.treachrousNrr = this.treacherousData.nrr;
      this.rangnaRockStarNrr = this.rangnaRockStarData.nrr;
    } else if (this.rangnaRockStarEightMatchForScore > this.treacherousEightMatchForScore) {
      // rangnarock Star won this game
      this.rangnaRockStarNrr = (this.rangnaRockStarForRuns / this.rangnaRockStarForOvers) - (this.rangnaRockStarAgainstRuns / this.rangnaRockStarAgainstOvers);
      this.treachrousNrr = (this.treachrousForRuns / this.treachrousForOvers) - (this.treachrousAgainstRuns / this.treachrousAgainstOvers);
    } else {
      // treachrous won this game
      this.rangnaRockStarNrr = (this.rangnaRockStarForRuns / this.rangnaRockStarForOvers) - (this.rangnaRockStarAgainstRuns / this.rangnaRockStarAgainstOvers);
      this.treachrousNrr = (this.treachrousForRuns / this.treachrousForOvers) - (this.treachrousAgainstRuns / this.treachrousAgainstOvers);
    }

    console.log("this.treachrousForRuns", this.treachrousForRuns);
    console.log("this.treachrousForOvers", this.treachrousForOvers);
    console.log("this.treachrousAgainstRuns", this.treachrousAgainstRuns);
    console.log("this.treachrousAgainstOvers", this.treachrousAgainstOvers);
    console.log("this.rangnaRockStarForRuns", this.rangnaRockStarForRuns);
    console.log("this.rangnaRockStarForOvers", this.rangnaRockStarForOvers);
    console.log("this.rangnaRockStarAgainstRuns", this.rangnaRockStarAgainstRuns);
    console.log("this.rangnaRockStarAgainstOvers", this.rangnaRockStarAgainstOvers);
    console.log("trechrousNrr", this.treachrousNrr.toFixed(3));
    console.log("this.rangnaRockStarNrr", this.rangnaRockStarNrr.toFixed(3));
  }
}
