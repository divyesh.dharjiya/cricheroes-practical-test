import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionOneAnswerOneComponent } from './question-one-answer-one.component';

describe('QuestionOneAnswerOneComponent', () => {
  let component: QuestionOneAnswerOneComponent;
  let fixture: ComponentFixture<QuestionOneAnswerOneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuestionOneAnswerOneComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionOneAnswerOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
