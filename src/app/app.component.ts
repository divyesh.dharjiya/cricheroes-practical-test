import { Component, OnInit } from '@angular/core';
import * as data from 'public/data.json';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  pointsTableData: any = [];

  constructor() {
    this.pointsTableData = data['default'];
  }

  ngOnInit() {}
}
