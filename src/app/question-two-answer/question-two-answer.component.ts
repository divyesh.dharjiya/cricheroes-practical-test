import { Component, OnInit } from '@angular/core';
import * as data from 'public/data.json';
@Component({
  selector: 'app-question-two-answer',
  templateUrl: './question-two-answer.component.html',
  styleUrls: ['./question-two-answer.component.scss'],
})
export class QuestionTwoAnswerComponent implements OnInit {
  // points table data variables
  pointsTableData: any = [];
  treacherousData: any = {};
  bhayankarXiData: any = {};
  // treachrous and BHAYANKAR XI scores data variables
  treacherousEightMatchForScore: number = 80;
  treacherousEightMatchForOvers: number = 17.333;
  bhayankarXiEightMatchForScore: number = 79;
  bhayankarXiEightMatchForOvers: number = 20;
  // treachrous for and against data variables
  treachrousForRuns: number;
  treachrousForOvers: number;
  treachrousAgainstRuns: number;
  treachrousAgainstOvers: number;
  // BHAYANKAR XI for and against data variables
  bhayankarXiForRuns: number;
  bhayankarXiForOvers: number;
  bhayankarXiAgainstRuns: number;
  bhayankarXiAgainstOvers: number;
  // treachrous and BHAYANKAR XI NRR variables
  treachrousNrr: number;
  bhayankarXiNrr: number;
  constructor() {
    this.pointsTableData = data['default'];
    this.pointsTableData.forEach((item) => {
      if (item.team === 'Treacherous') {
        this.treacherousData = item;
      } else if (item.team === 'BHAYANKAR XI') {
        this.bhayankarXiData = item;
      }
    });
  }

  ngOnInit(): void {
    this.getAllCalculations();
  }

  getAllCalculations() {
    this.treachrousForRuns =
      this.treacherousData.forRuns + this.treacherousEightMatchForScore;
    this.treachrousForOvers =
      this.treacherousData.forOvers + this.treacherousEightMatchForOvers;
    this.treachrousAgainstRuns =
      this.treacherousData.againstRuns + this.bhayankarXiEightMatchForScore;
    this.treachrousAgainstOvers =
      this.treacherousData.againstOvers + this.bhayankarXiEightMatchForOvers;
    this.bhayankarXiForRuns =
      this.bhayankarXiData.forRuns + this.bhayankarXiEightMatchForScore;
    this.bhayankarXiForOvers =
      this.bhayankarXiData.forOvers + this.bhayankarXiEightMatchForOvers;
    this.bhayankarXiAgainstRuns =
      this.bhayankarXiData.againstRuns + this.treacherousEightMatchForScore;
    this.bhayankarXiAgainstOvers =
      this.bhayankarXiData.againstOvers + this.treacherousEightMatchForOvers;

    if (this.bhayankarXiEightMatchForScore === this.treacherousEightMatchForScore) {
      // match Tied
      console.log('match tied');
      this.treachrousNrr = this.treacherousData.nrr;
      this.bhayankarXiNrr = this.bhayankarXiData.nrr;
    } else if (this.bhayankarXiEightMatchForScore > this.treacherousEightMatchForScore) {
      // BHAYANKAR XI Star won this game
      this.bhayankarXiNrr = (this.bhayankarXiForRuns / this.bhayankarXiForOvers) - (this.bhayankarXiAgainstRuns / this.bhayankarXiAgainstOvers);
      this.treachrousNrr = (this.treachrousForRuns / this.treachrousForOvers) - (this.treachrousAgainstRuns / this.treachrousAgainstOvers);
    } else {
      // treachrous won this game
      console.log('1', this.treachrousForRuns);
      console.log('2', this.treachrousForOvers);
      console.log('3', this.treachrousAgainstRuns);
      console.log('4', this.treachrousAgainstOvers);
      this.bhayankarXiNrr = (this.bhayankarXiForRuns / this.bhayankarXiForOvers) - (this.bhayankarXiAgainstRuns / this.bhayankarXiAgainstOvers);
      this.treachrousNrr = (this.treachrousForRuns / this.treachrousForOvers) - (this.treachrousAgainstRuns / this.treachrousAgainstOvers);
    }

    console.log('this.treachrousForRuns', this.treachrousForRuns);
    console.log('this.treachrousForOvers', this.treachrousForOvers);
    console.log('this.treachrousAgainstRuns', this.treachrousAgainstRuns);
    console.log('this.treachrousAgainstOvers', this.treachrousAgainstOvers);
    console.log('this.bhayankarXiForRuns', this.bhayankarXiForRuns);
    console.log('this.bhayankarXiForOvers', this.bhayankarXiForOvers);
    console.log('this.bhayankarXiAgainstRuns', this.bhayankarXiAgainstRuns);
    console.log('this.bhayankarXiAgainstOvers', this.bhayankarXiAgainstOvers);
    console.log('trechrousNrr', this.treachrousNrr.toFixed(3));
    console.log('this.bhayankarXiNrr', this.bhayankarXiNrr.toFixed(3));
  }
}
