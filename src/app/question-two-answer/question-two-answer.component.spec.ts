import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionTwoAnswerComponent } from './question-two-answer.component';

describe('QuestionTwoAnswerComponent', () => {
  let component: QuestionTwoAnswerComponent;
  let fixture: ComponentFixture<QuestionTwoAnswerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuestionTwoAnswerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionTwoAnswerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
