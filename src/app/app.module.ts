import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { QuestionOneAnswerOneComponent } from './question-one-answer-one/question-one-answer-one.component';
import { QuestionTwoAnswerComponent } from './question-two-answer/question-two-answer.component';

@NgModule({
  declarations: [
    AppComponent,
    QuestionOneAnswerOneComponent,
    QuestionTwoAnswerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
