import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { QuestionOneAnswerOneComponent } from './question-one-answer-one/question-one-answer-one.component';
import { QuestionTwoAnswerComponent } from './question-two-answer/question-two-answer.component';

const routes: Routes = [
  {
    path: 'question-one-answer',
    component: QuestionOneAnswerOneComponent
  }, {
    path: 'question-two-answer',
    component: QuestionTwoAnswerComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
